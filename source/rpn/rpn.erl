-module(rpn).

-export([parse_and_run/1, parse/1, run/1]).

parse_and_run(Exp) ->
    run(parse(Exp)).

parse(Exp) when is_list(Exp) ->
    string:tokens(Exp, " ").

run(Exp) when is_list(Exp) ->
    run(Exp, []).

run(["+"|T], [A, B|C]) ->
    run(T, [A + B|C]);
run(["-"|T], [A, B|C]) ->
    run(T, [A - B|C]);
run(["*"|T], [A, B|C]) ->
    run(T, [A * B|C]);
run(["/"|T], [A, B|C]) ->
    run(T, [A div B|C]);
run(["%"|T], [A, B|C]) ->
    run(T, [A rem B|C]);
run(["^"|T], [A, B|C]) ->
    run(T, [math:pow(A, B)|C]);
run([H|T], Q) ->
    try list_to_integer(H) of
        A -> run(T, [A|Q])
    catch
        error:_ ->
            try list_to_float(H) of
                A -> run(T, [A|Q])
            catch
                error:_ -> {unknown, H}
            end
    end;
run([], [H|[]]) ->
    {ok, H};
run([], [H|T]) ->
    {ok, H, T}.
