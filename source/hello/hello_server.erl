-module(hello_server).

-export([start/0, loop/0]).

-import(hello, [hello/1, world/0, all/1]).

start() ->
    spawn(?MODULE, loop, []).

loop() ->
    receive
        {From, {hello, To}} ->
            From ! {self(), hello(To)},
            loop();

        {From, {world}} ->
            From ! {self(), world()},
            loop();

        {From, {all, All}} ->
            From ! {self(), all(All)},
            loop();

        {From, {stop}} ->
            From ! {self(), stopped};

        {From, Other} ->
            From ! {self(), {unknown, Other}},
            loop()
    end.
