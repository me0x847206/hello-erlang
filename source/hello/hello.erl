-module(hello).

-export([hello/1, world/0, all/1, all/2, print/2, print/3]).

hello(Target) when is_list(Target) ->
    "Hello, " ++ Target ++ "!".

world() ->
	[hello("World")].

all(All) ->
    all(All, fifo).

all(All, Order) ->
    case Order of
        fifo -> lists:reverse(all_acc(All, []));
        lifo -> all_acc(All, []);
        Other -> {unknown, Other}
    end.

all_acc([], Acc) ->
    Acc;
all_acc([H|T], Acc) ->
    all_acc(T, [hello(H)|Acc]).

print([], _Suffix) ->
    true;
print([H|T], Suffix) ->
    io:fwrite("~s~s", [H, Suffix]),
    print(T, Suffix).

print([], _Prefix, _Suffix) ->
    true;
print([H|T], Prefix, Suffix) ->
    io:fwrite("~s~s~s", [Prefix, H, Suffix]),
    print(T, Prefix, Suffix).
